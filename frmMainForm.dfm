object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Editor de texto'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 635
    Height = 299
    Align = alClient
    HideSelection = False
    ScrollBars = ssVertical
    TabOrder = 0
    OnChange = Memo1Change
  end
  object MainMenu1: TMainMenu
    Left = 536
    Top = 64
    object File1: TMenuItem
      Caption = 'Archivo'
      object Nuevo1: TMenuItem
        Action = NuevoFichero
      end
      object Open1: TMenuItem
        Action = FileOpen1
      end
      object Guardar2: TMenuItem
        Action = GuardarFichero
      end
      object Guardar1: TMenuItem
        Action = FileSaveAs1
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Salir1: TMenuItem
        Action = FileExit1
      end
    end
    object Edicin1: TMenuItem
      Caption = 'Edici'#243'n'
      object Undo1: TMenuItem
        Action = EditUndo1
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Cut1: TMenuItem
        Action = EditCut1
      end
      object Copy1: TMenuItem
        Action = EditCopy1
      end
      object Paste1: TMenuItem
        Action = EditPaste1
      end
      object Delete1: TMenuItem
        Action = EditDelete1
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Find1: TMenuItem
        Action = SearchFind1
      end
      object FindNext1: TMenuItem
        Action = SearchFindNext1
      end
      object Replace1: TMenuItem
        Action = SearchReplace1
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object SelectAll1: TMenuItem
        Action = EditSelectAll1
      end
      object Horayfecha1: TMenuItem
        Action = FechaHora
      end
    end
    object Formato1: TMenuItem
      Caption = 'Formato'
      object Ajustedelnea1: TMenuItem
        Action = AjusteLinea
      end
      object SelectFont1: TMenuItem
        Action = DialogFontEdit1
      end
    end
    object Ayuda1: TMenuItem
      Caption = 'Ayuda'
      object Acercade1: TMenuItem
        Caption = 'Acerca de...'
        OnClick = Acercade1Click
      end
    end
  end
  object ActionList1: TActionList
    Left = 536
    Top = 8
    object FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Abrir...'
      Dialog.DefaultExt = 'txt'
      Dialog.Filter = 'Ficheros de texto|*.txt|Todos los ficheros|*.*'
      Hint = 'Abre un archivo existente'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen1Accept
    end
    object FileExit1: TFileExit
      Category = 'File'
      Caption = 'Salir'
      Hint = 'Exit|Quits the application'
      ImageIndex = 43
    end
    object NuevoFichero: TAction
      Category = 'File'
      Caption = 'Nuevo'
      ShortCut = 16462
      OnExecute = NuevoFicheroExecute
    end
    object FileSaveAs1: TFileSaveAs
      Category = 'File'
      Caption = 'Guardar &como...'
      Dialog.DefaultExt = 'txt'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
      OnAccept = FileSaveAs1Accept
    end
    object GuardarFichero: TAction
      Category = 'File'
      Caption = 'Guardar'
      ShortCut = 16467
      OnExecute = GuardarFicheroExecute
    end
    object EditCut1: TEditCut
      Category = 'Edit'
      Caption = 'Cortar'
      Hint = 'Cut|Cuts the selection and puts it on the Clipboard'
      ImageIndex = 0
      ShortCut = 16472
    end
    object EditCopy1: TEditCopy
      Category = 'Edit'
      Caption = 'Copiar'
      Hint = 'Copy|Copies the selection and puts it on the Clipboard'
      ImageIndex = 1
      ShortCut = 16451
    end
    object EditPaste1: TEditPaste
      Category = 'Edit'
      Caption = 'Pegar'
      Hint = 'Paste|Inserts Clipboard contents'
      ImageIndex = 2
      ShortCut = 16470
    end
    object EditSelectAll1: TEditSelectAll
      Category = 'Edit'
      Caption = 'Seleccionar todo'
      Hint = 'Select All|Selects the entire document'
      ShortCut = 16449
    end
    object EditUndo1: TEditUndo
      Category = 'Edit'
      Caption = 'Deshacer'
      Hint = 'Undo|Reverts the last action'
      ImageIndex = 3
      ShortCut = 16474
    end
    object EditDelete1: TEditDelete
      Category = 'Edit'
      Caption = 'Eliminar'
      Hint = 'Delete|Erases the selection'
      ImageIndex = 5
      ShortCut = 46
    end
    object SearchFind1: TSearchFind
      Category = 'Search'
      Caption = 'Buscar...'
      Hint = 'Find|Finds the specified text'
      ImageIndex = 34
      ShortCut = 16454
    end
    object SearchFindNext1: TSearchFindNext
      Category = 'Search'
      Caption = 'Buscar siguiente...'
      Hint = 'Find Next|Repeats the last find'
      ImageIndex = 33
      ShortCut = 114
    end
    object SearchReplace1: TSearchReplace
      Category = 'Search'
      Caption = 'Reemplazar...'
      Hint = 'Replace|Replaces specific text with different text'
      ImageIndex = 32
    end
    object AjusteLinea: TAction
      Category = 'File'
      Caption = 'Ajuste de l'#237'nea'
      Checked = True
      OnExecute = AjusteLineaExecute
    end
    object DialogFontEdit1: TFontEdit
      Category = 'Dialog'
      Caption = 'Fuente...'
      Dialog.Font.Charset = DEFAULT_CHARSET
      Dialog.Font.Color = clWindowText
      Dialog.Font.Height = -11
      Dialog.Font.Name = 'Tahoma'
      Dialog.Font.Style = []
      Hint = 'Font Select'
      OnAccept = DialogFontEdit1Accept
    end
    object FechaHora: TAction
      Category = 'Dialog'
      Caption = 'Hora y fecha'
      OnExecute = FechaHoraExecute
    end
  end
end
