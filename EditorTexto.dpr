program EditorTexto;

uses
  Vcl.Forms,
  frmMainForm in 'frmMainForm.pas' {MainForm},
  frmAcerca in 'frmAcerca.pas' {AcercaDe},
  uNombreFichero in 'uNombreFichero.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
