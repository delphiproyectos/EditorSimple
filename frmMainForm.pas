unit frmMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, System.Actions,
  Vcl.ActnList, Vcl.StdActns, uNombreFichero;

type
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Salir1: TMenuItem;
    Ayuda1: TMenuItem;
    Acercade1: TMenuItem;
    Memo1: TMemo;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    Open1: TMenuItem;
    N1: TMenuItem;
    FileExit1: TFileExit;
    Nuevo1: TMenuItem;
    NuevoFichero: TAction;
    FileSaveAs1: TFileSaveAs;
    Guardar1: TMenuItem;
    Guardar2: TMenuItem;
    GuardarFichero: TAction;
    Edicin1: TMenuItem;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    EditSelectAll1: TEditSelectAll;
    EditUndo1: TEditUndo;
    EditDelete1: TEditDelete;
    Cut1: TMenuItem;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    Delete1: TMenuItem;
    Undo1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    SelectAll1: TMenuItem;
    SearchFind1: TSearchFind;
    SearchFindNext1: TSearchFindNext;
    SearchReplace1: TSearchReplace;
    N4: TMenuItem;
    Find1: TMenuItem;
    FindNext1: TMenuItem;
    Replace1: TMenuItem;
    Formato1: TMenuItem;
    AjusteLinea: TAction;
    Ajustedelnea1: TMenuItem;
    DialogFontEdit1: TFontEdit;
    SelectFont1: TMenuItem;
    FechaHora: TAction;
    Horayfecha1: TMenuItem;
    procedure Acercade1Click(Sender: TObject);
    procedure FileOpen1Accept(Sender: TObject);
    procedure NuevoFicheroExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FileSaveAs1Accept(Sender: TObject);
    procedure GuardarFicheroExecute(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure AjusteLineaExecute(Sender: TObject);
    procedure DialogFontEdit1Accept(Sender: TObject);
    procedure FechaHoraExecute(Sender: TObject);
  private
    FNombreFichero: TNombreFichero;
    function GuardarArchivo: Boolean;
    procedure EscribeArchivo;
    procedure LimpiarMemo;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  frmAcerca;

procedure TMainForm.Acercade1Click(Sender: TObject);
var
  AcercaDe: TAcercaDe;
begin
  AcercaDe := TAcercaDe.Create(Self);
  try
    AcercaDe.ShowModal;
  finally
    AcercaDe.Free;
  end;
end;

procedure TMainForm.FechaHoraExecute(Sender: TObject);
begin
  Memo1.SelText := DateTimeToStr(Now);
end;

procedure TMainForm.FileOpen1Accept(Sender: TObject);
begin
  FNombreFichero.NombreFichero := FileOpen1.Dialog.FileName;
  if FileExists(FNombreFichero.NombreFichero) then
  begin
    Memo1.Lines.LoadFromFile(FNombreFichero.NombreFichero);
  end;
  FNombreFichero.EstaEditado := False;
end;

procedure TMainForm.FileSaveAs1Accept(Sender: TObject);
begin
  FNombreFichero.NombreFichero := FileSaveAs1.Dialog.FileName;
  FNombreFichero.EstaEditado := False;
  Memo1.Lines.SaveToFile(FNombreFichero.NombreFichero);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FNombreFichero := TNombreFichero.Crear;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FNombreFichero.Free;
end;

procedure TMainForm.GuardarFicheroExecute(Sender: TObject);
begin
  GuardarArchivo;
end;

procedure TMainForm.NuevoFicheroExecute(Sender: TObject);
begin
  if FNombreFichero.DebeGuardar then
  begin
    if GuardarArchivo then
    begin
      LimpiarMemo;
    end;
  end else
  begin
    LimpiarMemo;
  end;
end;

function TMainForm.GuardarArchivo: Boolean;
var
  TempNombreFichero: string;
begin
  Result := False;
  if FNombreFichero.HaSidoGuardado then
  begin
    EscribeArchivo;
    Result := True;
  end else
  begin
    FileSaveAs1.Execute;
    TempNombreFichero := FileSaveAs1.Dialog.FileName;
    if TempNombreFichero <> '' then
    begin
      FNombreFichero.NombreFichero := TempNombreFichero;
      EscribeArchivo;
      Result := True;
    end;
  end;
end;

procedure TMainForm.AjusteLineaExecute(Sender: TObject);
begin
  Memo1.WordWrap := not Memo1.WordWrap;
  AjusteLinea.Checked := Memo1.WordWrap;
  if Memo1.WordWrap then
  begin
    Memo1.ScrollBars := ssVertical;
  end else
  begin
    Memo1.ScrollBars := ssBoth;
  end;
end;

procedure TMainForm.DialogFontEdit1Accept(Sender: TObject);
begin
  Memo1.Font := DialogFontEdit1.Dialog.Font;
end;

procedure TMainForm.EscribeArchivo;
begin
  Memo1.Lines.SaveToFile(FNombreFichero.NombreFichero);
  FNombreFichero.EstaEditado := False;
end;

procedure TMainForm.LimpiarMemo;
begin
  Memo1.Clear;
  FNombreFichero.Limpiar;
end;

procedure TMainForm.Memo1Change(Sender: TObject);
begin
  FNombreFichero.EstaEditado := True;
end;

end.
