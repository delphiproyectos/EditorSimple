unit uNombreFichero;

interface

type
  TNombreFichero = class(TObject)
  private
    FEstaEditado: Boolean;
    FNombreFichero: string;
    procedure SetEstaEditado(const Value: Boolean);
    procedure SetNombreFichero(const Value: string);
    procedure Iniciar;
    function GetNombreFichero: string;
    function GetHaSidoGuardado: Boolean;
  public
    constructor Crear;
    constructor CreateConNombre(aNombreFichero: string);
    procedure Limpiar;
    function DebeGuardar: Boolean;
  published
    property HaSidoGuardado: Boolean read GetHaSidoGuardado;
    property EstaEditado: Boolean read FEstaEditado write SetEstaEditado;
    property NombreFichero: string read GetNombreFichero write SetNombreFichero;
  end;

implementation

uses
  Dialogs, Controls;

{ TNombreFichero }

constructor TNombreFichero.Crear;
begin
  inherited Create;
  Iniciar;
end;

constructor TNombreFichero.CreateConNombre(aNombreFichero: string);
begin
  inherited;
  Iniciar;
  FNombreFichero := aNombreFichero;
end;

function TNombreFichero.DebeGuardar: Boolean;
var
  TempVentana: string;
  TempRespuesta: integer;
begin
  Result := False;
  if EstaEditado then
  begin
    TempVentana := '�Desea guardar los cambios hechos a ' + FNombreFichero;
    TempRespuesta := MessageDlg(TempVentana, mtConfirmation, [mbYes, mbNo, mbCancel], 0);
    if TempRespuesta = mrNo then
    begin
      EstaEditado := False;
    end;
    Result := (TempRespuesta = mrYes);
  end;
end;

function TNombreFichero.GetHaSidoGuardado: Boolean;
begin
  Result := FNombreFichero <> ''; 
end;

function TNombreFichero.GetNombreFichero: string;
begin
if FNombreFichero = '' then
  begin
    Result := 'SinTitulo.txt';    
  end else
  begin
    Result := FNombreFichero;  
  end;
end;

procedure TNombreFichero.Iniciar;
begin
  EstaEditado := True;
  FNombreFichero := '';
end;

procedure TNombreFichero.Limpiar;
begin
  Iniciar;
end;

procedure TNombreFichero.SetEstaEditado(const Value: Boolean);
begin
  FEstaEditado := Value;
end;

procedure TNombreFichero.SetNombreFichero(const Value: string);
begin
  FNombreFichero := Value;
end;

end.
